using SubSys_SimDriving;

namespace SubSys_SimDriving
{
	public abstract class MobileEntity : TrafficEntity
	{
        /// <summary>
        /// 当前车辆的速度
        /// </summary>
		internal int iSpeed;
		 
	}
	 
}
 
