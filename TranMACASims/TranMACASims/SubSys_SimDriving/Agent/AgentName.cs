using SubSys_SimDriving;

namespace SubSys_SimDriving.Agents
{
	internal static class AgentName
	{
        internal  const string VMSAgent = "VMSAgent";
        internal  const string DecelerateAgent = "DecelerateAgent";
        internal  const string CollisionAvoidingAgent = "CollisionAvoidingAgent";
        internal  const string LaneShiftAgent = "LaneShiftAgent";
        internal const string SpeedUpDownAgent = "SpeedUpDownAgent";

        internal  const string TrafficLightAgent="TrafficLightAgent";
	}
	 
}
 
